import { defineCliConfig } from "sanity/cli"

export default defineCliConfig({
  api: {
    projectId: "2a5foyya",
    dataset: "production",
  },
})
